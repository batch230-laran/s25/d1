console.log("Hello world");



// JSON OBJECT


/*

- JSON stands for JavaScript Object Notation
- JSON is alos used in other programming languages 
- Core JavaScript has a built in JSON Objects that contains methods for passing JSON objects
- JSON is used for serializing different data types into bytes
- JSON also used the "key/value pairs" just like the object properties in JavaScript
- Key/Propery names should be enclosed with double quotes ("")


Syntax:
	{
		"propertyA" : "valueA",
		"propertyB"	: "valueB"
	}


{

	"city" : "Quexon City",
	"province" : "Metro Manila",
	"country" : "Philippines" 

}


*/



// JSON ARRAYS


// Array in JSON are almost same as arrayss in JavaScript


/*

Arrays in JSON Object


	"cities" : [
		{
			"city" : "Quexon City",
			"province" : "Metro Manila",
			"country" : "Philippines" 
		},
		{
			"city" : "Manila City",
			"province" : "Metro Manila",
			"country" : "Philippines" 
		}, 
		{
			"city" : "Makati City",
			"province" : "Metro Manila",
			"country" : "Philippines" 
		}
	]

*/







// ----------------------------------------



// JSON METHODS


// The "JSON Object" contains methods for parsing and conversing data in stringified JSON

// JSON data is sent or received in text-only(string) format







// ----------------------------------------



// CONVERTING DATA INTO STRINGIFIED JSON





console.log("------------------ CONVERTING DATA INTO STRINGIFIED JSON");


let batchesArr = [
	{
		batchName: 230,
		schedule: "Part Time"
	},
	{
		batchName: 240,
		schedule: "Full Time"
	}
];

console.log(batchesArr);

console.log("Result from stringify method: ");
console.log(JSON.stringify(batchesArr));






// ANOTHER EXAMPLE




console.log("------------------ another example w/ diff style");

let data = JSON.stringify(
{
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});

console.log(data);





// ANOTHER EXAMPLE



console.log("------------------ mini activity");



let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ");



let login = JSON.stringify(
{
	firstName: firstName,
	lastName: lastName,
	email: email,
	password: password

}
);

console.log(login);









// ----------------------------------------



// CONVERTING STRINGIFIED JSON into javascript objects




console.log("------------------ CONVERTING STRINGIFIED JSON into JAVASCRIPT OBJECT");



let batchesJSON = `[
	{
		"batchName" : 230,
		"schedule" : "Part Time"
	},
	{
		"batchName" : 240,
		"schedule" : "Full Time"
	}
]`;

console.log("batchesJSON content: ");
console.log(batchesJSON);


console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);





// ANOTHER EXAMPLE



let stringifiedObject = `
	{
		"name" : "John",
		"age" : 31,
	 	"address" : {
			"city" : "Manila",
			"country" : "Philippines"
		}
	}
`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));





















































































